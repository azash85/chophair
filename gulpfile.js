var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')
var rename = require('gulp-rename')
var minifyCss = require('gulp-minify-css')

gulp.task('js', function () {
  gulp.src(['src/app/app.js', 'src/app/**/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
})

gulp.task('watch', ['js', 'minify-css', 'copy-img', 'copy-font'], function () {
  gulp.watch('src/app/**/*.js', ['js'])
  gulp.watch('src/css/*.css', ['minify-css'])
  gulp.watch('src/img/**/*', ['copy-img'])
  gulp.watch('src/font/**/*', ['copy-font'])
})

gulp.task('minify-css', function() {
    return gulp.src('src/css/*.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/css'));
})

gulp.task('copy-img', function () {
    return gulp.src(['src/img/*'], {
        base: 'src'
    }).pipe(gulp.dest('dist'));
})

gulp.task('copy-font', function () {
    return gulp.src(['src/font/*'], {
        base: 'src'
    }).pipe(gulp.dest('dist'));
})

gulp.task('default', ['watch'])
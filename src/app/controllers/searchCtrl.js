angular.module('mainApp')
.controller('searchCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams) {
    $scope.param = $routeParams.postcode;
    
    $http({
        url: baseUrl+'client/search',
        method: 'POST',
        params: {post_code: $scope.param}
    }).success(function(result) {
        $scope.shops = result;
        console.log(result);
    }).error(function(error) {
        console.log(error);
    });
    
    $scope.sendToShop = function(shop_name){
        $location.path('/'+shop_name);
    }
})
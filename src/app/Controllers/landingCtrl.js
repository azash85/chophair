angular.module('mainApp')
.controller('landingCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams) {
    
    $scope.searchPostcode = function(param){
        if(param){
            $location.path('/search/'+param);
        }
    }
})
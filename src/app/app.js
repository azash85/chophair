angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']).

constant( 'baseUrl', 'http://api.chophair.dev/' ).


config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider) {
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    
    //for CORS-issue
//    $httpProvider.defaults.headers.post = {
//        "Content-Type": "application/json",
//        "X-Requested-With": "XMLHttpRequest"
//    };

    $routeProvider
    .when('/', { 
        controller: 'landingCtrl', 
        templateUrl: 'src/app/views/landing.html'
    })
    .when('/search/:postcode', { 
        controller: 'searchCtrl', 
        templateUrl: 'src/app/views/search.html'
    })
    .when('/:shop', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html'
    }) 
    .when('/test', { 
        controller: 'testCtrl', 
        templateUrl: 'src/app/views/test.html' 
    })
    .otherwise({ redirectTo: '/' });
//    .otherwise({ redirectTo: function(){window.location = 'http://www.tailify.com';} });

});
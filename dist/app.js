angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']).

constant( 'baseUrl', 'http://api.chophair.dev/' ).


config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider) {
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    
    //for CORS-issue
//    $httpProvider.defaults.headers.post = {
//        "Content-Type": "application/json",
//        "X-Requested-With": "XMLHttpRequest"
//    };

    $routeProvider
    .when('/', { 
        controller: 'landingCtrl', 
        templateUrl: 'src/app/views/landing.html'
    })
    .when('/search/:postcode', { 
        controller: 'searchCtrl', 
        templateUrl: 'src/app/views/search.html'
    })
    .when('/:shop', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html'
    }) 
    .when('/test', { 
        controller: 'testCtrl', 
        templateUrl: 'src/app/views/test.html' 
    })
    .otherwise({ redirectTo: '/' });
//    .otherwise({ redirectTo: function(){window.location = 'http://www.tailify.com';} });

});
angular.module('mainApp')
.controller('homeCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams, $modal) {
    $scope.shop_name = $routeParams.shop;
    $scope.today_date = new Date().toISOString().split("T")[0]; //for date-picker min-date
    $scope.booking_date = new Date();
    $scope.shop = '';
    $scope.selected_service = '';
    $scope.selected_time = '';
    $scope.time_selection = '';
    $scope.date_for_modal = '';
    $scope.card = [];
    $scope.loaded = false;
    
    $http({
        url: baseUrl+'client/info',
        method: 'POST',
        params: {profile_name: $scope.shop_name}
    }).success(function(result) {
        $scope.shop = result;
        $scope.datePicked($scope.booking_date);
        $scope.selected_service = Object.keys($scope.shop.services)[0];
        $scope.selected_time = $scope.time_selection[0];
    });
    
    $scope.datePicked = function(date){
        var day = getDayFromDate(date);
        if($scope.shop.opening_times[day] != 'Closed'){
            var open_hours = $scope.shop.opening_times[day];
            $scope.time_selection = createTimeArr(open_hours, '15');
            $scope.selected_time = $scope.time_selection[0];
        }
        else{
            $scope.time_selection = ['Closed'];
            $scope.selected_time = 'Closed';
        }
    }
    
    $scope.book = function(){
        addTimeToBooking($scope.selected_time);
        $scope.date_for_modal = formatDateForModal($scope.booking_date);
        openModal('src/app/views/booking_modal.html');
    }
    
    $scope.imageLoaded = function(){
        $scope.loaded = true;
    }
    
    function formatDateForModal(date){
        var format = getDayFromDate(date);
        format = format + ' ' +date.getFullYear();
        format = format + '-' +('0'+(date.getMonth()+1)).slice(-2);
        format = format + '-' +('0'+date.getDate()).slice(-2);
        return format;
    }
    function getDayFromDate(date){
        var weekday = new Array(7);
        weekday[0]=  "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        
        return weekday[date.getDay()];
    }
    
    function addTimeToBooking(time){
        var hours = time.substring(0, 2);
        var minutes = time.substring(3, 5);
        $scope.booking_date.setHours(hours);
        $scope.booking_date.setMinutes(minutes);
    }
    
    function createTimeArr(open_hours, interval){
        var open = open_hours.substring(0, 5);
        var close = open_hours.substring(8, 13);
        var date = new Date();
        date.setMinutes(open.substring(3,5));
        date.setHours(parseInt(open.substring(0,2))); //set the datetime object to open
        var count = (parseInt(close.substring(0,2))-parseInt(open.substring(0,2))) * 4;
        var arr = [];
        arr.push(open);
        for(var i=1;i<=count;i++){
            date.setMinutes(date.getMinutes() + parseInt(interval));
            var new_time;
            date.getMinutes() == 0 ? new_time = date.getHours() + ':' + '00' : new_time = date.getHours() + ':' + date.getMinutes();
            new_time.length == 4 ? new_time = '0'+new_time : new_time = new_time;
            if(new_time < close){
                arr.push(new_time);
            }
        }
        return arr;
    }
    
    //MODAL SETUP
    function openModal(url) {
        var modalInstance = $modal.open({
            templateUrl: url,
            scope: $scope,
            size: 'lg',
            backdrop: 'trus', //'static' if modal shouldnt close when clicking outside of modal.
            controller: function($scope, $modalInstance, $location, $http, $timeout) {

                $scope.ok = function () {
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.bookConfirm = function(card){
                    $scope.loaded = false;
                    $http({
                        url: baseUrl+'client/booking',
                        method: 'POST',
                        params: {client_id: $scope.shop.id, card_holder_name: card.name, cvv: card.cvc, expire_year: card.expire_year, expire_month: card.expire_month, card_number: card.number, user_email: card.email, time: $scope.selected_time, date: $scope.date_for_modal, price: $scope.shop.services[$scope.selected_service]}
                    }).success(function(result) {
                        $timeout(function(){
                            $scope.success = result;
                            $scope.loaded = true;
                        }, 2000);
                    });
                }
                $scope.forward = function(){
                    $modalInstance.close();
                }

            }
        });
        modalInstance.result.then(function () {
//            console.log('Modal accepted');
        }, function () {
//            console.log('Modal dismissed at: ' + new Date());
        });
    };

})
angular.module('mainApp')
.controller('landingCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams) {
    
    $scope.searchPostcode = function(param){
        if(param){
            $location.path('/search/'+param);
        }
    }
})
angular.module('mainApp')
.controller('navCtrl', function ($scope, $location, $rootScope) {

})
angular.module('mainApp')
.controller('searchCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams) {
    $scope.param = $routeParams.postcode;
    
    $http({
        url: baseUrl+'client/search',
        method: 'POST',
        params: {post_code: $scope.param}
    }).success(function(result) {
        $scope.shops = result;
        console.log(result);
    }).error(function(error) {
        console.log(error);
    });
    
    $scope.sendToShop = function(shop_name){
        $location.path('/'+shop_name);
    }
})
angular.module('mainApp')
.controller('testCtrl', function ($scope, $location, $rootScope, baseUrl, $http, $routeParams) {

})
angular.module('mainApp')
//<img ng-src="{{src}}" imageonload="doThis()" />
.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
})
.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});